import React from 'react';
import Navigation from "./src/components/navidation";
import {AuthProvider} from "./src/firebaseAuth/AuthProvider";

const App = () => {

    return (
        <AuthProvider>
            <Navigation/>
        </AuthProvider>
    );
};

export default App;
