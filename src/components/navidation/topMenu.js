import React, {Component} from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

export default class TopMenu extends Component {

    toggleDrawer = () => {
        let {navigationProps} = this.props;
        navigationProps.openDrawer();
    }

    goBack = () => {
        let {goBackAgrooperation, navigationBack, geozoneDetail} = this.props;
        if (goBackAgrooperation === true && !geozoneDetail) {
            navigationBack.navigate("AgroOperations", {
                updateData: false
            }).then(() => {
            });
        } else {
            navigationBack.goBack();
        }
    }

    render() {
        let {
            navigationProps,
            navigationBack,
            logout
        } = this.props;
        return (
            <View style={styles.container}>
                {
                    navigationProps &&
                    <TouchableOpacity onPress={this.toggleDrawer.bind(this)}>
                        <Icon name={"menu"} style={styles.icon} size={35}/>
                    </TouchableOpacity>
                }
                {
                    navigationBack &&
                    <TouchableOpacity onPress={this.goBack.bind(this)}>
                        <Icon name={"arrow-left"} style={styles.icon} size={30}/>
                    </TouchableOpacity>
                }
                <View style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                    width: navigationBack || navigationProps ? '80%' : "100%",

                }}>
                    <Image
                        source={require("../../assets/logo.png")}
                        style={{height: 30, width: 30}}/>
                    <Text style={styles.headerText}>
                        <Text style={{color: '#2b75e5'}}>U</Text>
                        <Text style={{color: '#f9d548'}}>A</Text> Family
                    </Text>
                </View>
                {logout &&
                    <TouchableOpacity
                        style={{position: "absolute", right: 10}}
                        onPress={() => {
                            logout();
                        }}
                    >
                        <Image
                            style={{height: 30, width: 30}}
                            source={require("../../assets/logoutIcon.png")}
                        />
                    </TouchableOpacity>
                }
            </View>
        );
    }
}

const styles = StyleSheet.create({
        container: {
            flexDirection: 'row',
            backgroundColor: "#6495EDFF",
            elevation: 10,
            alignItems: 'center',
            height: 50,
            width: '100%'
        },
        icon: {
            marginLeft: 10
        },
        headerText: {
            fontSize: 20,
            marginLeft: 5,
            marginBottom: 3,
            fontWeight: "bold",
            color: 'white'
        },
    }
)
