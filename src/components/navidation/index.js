import React, {useEffect, useState} from 'react';
import {NavigationContainer} from "@react-navigation/native";

import {createStackNavigator} from "@react-navigation/stack";
import BottomTabs from "../../screens";
import AddNewPeoples from "../../screens/AddNewPeoplesList/AddNewPeoples";
import auth from "@react-native-firebase/auth";
import {Login} from "../../screens/Login";
import {CreateAccount} from "../../screens/CreateAccount";

const Stack = createStackNavigator();
export default function Navigation() {
    const [initializing, setInitializing] = useState(true);
    const [user, setUser] = useState();

    function onAuthStateChanged(user) {
        setUser(user);
        if (initializing) setInitializing(false);
    }

    useEffect(() => {
        return auth().onAuthStateChanged(onAuthStateChanged); // unsubscribe on unmount
    }, []);

    if (initializing) return null;
    return (
        <NavigationContainer>
            {user ?
                <Stack.Navigator>
                    <Stack.Screen name="Main"
                                  component={BottomTabs}
                                  options={{headerShown: false}}>
                    </Stack.Screen>
                    <Stack.Screen name="AddNewPeoples"
                                  component={AddNewPeoples}
                                  options={{headerShown: false}}>
                    </Stack.Screen>
                </Stack.Navigator>
                :
                <Stack.Navigator>
                    <Stack.Screen name="Login"
                                  component={Login}
                                  options={{headerShown: false}}>
                    </Stack.Screen>
                    <Stack.Screen name="CreateAccount"
                                  component={CreateAccount}
                                  options={{headerShown: false}}>
                    </Stack.Screen>
                </Stack.Navigator>
            }
        </NavigationContainer>
    );
}
