import React, {PureComponent} from "react";
import {
    FlatList,
    I18nManager,
    Modal,
    Platform,
    StyleSheet,
    TextInput,
    TouchableOpacity,
    View,
    Text,
    KeyboardAvoidingView
} from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import styled from "styled-components";
import Entypo from "react-native-vector-icons/Entypo";
import Highlighter from "react-native-highlight-words";

export default class SearchableSelect extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            searchText: '',
            dataSourceState: props.dataSource,
        }
    }

    _searchFilterFunction(searchText, data) {
        let {dataSource} = this.state;
        let newData = [];
        if (searchText) {
            newData = data.filter(function (item) {
                const itemData = item.name.toUpperCase();
                const textData = searchText.toUpperCase();
                return itemData.includes(textData);
            });
            this.setState({
                dataSourceState: [...newData],
                searchText: searchText
            });
        } else {
            this.setState({dataSourceState: dataSource, searchText: searchText});
        }
    }

    render() {
        const {
            placeholder,
            selectedLabel,
            dataSource,
            disablePicker,
            changeAnimation,
            close,
            small,
            short,
            selectedValue,
            hideDetail,
            selectedColorLabel,
            checkInput,
            searchBarPlaceHolder
        } = this.props
        let {searchText, modalVisible, dataSourceState} = this.state;
        const specialKey = ["[", "?", "+", "*", "(", ")"];
        for (let i = 0; i < specialKey.length; i++) {
            searchText = searchText.replace(specialKey[i], '\\' + specialKey[i]);
        }
        return (
            <View>
                <SelectView>
                    <SelectLabelTouchableOpacity
                        close={close}
                        disabled={disablePicker}
                        onPress={() => {
                            this.setState({modalVisible: true});
                            if (checkInput) {
                                checkInput();
                            }
                        }}
                        activeOpacity={0.7}
                    >
                        {selectedLabel ?
                            <SelectedLabelText numberOfLines={2} selectedColorLabel={selectedColorLabel}
                                               disablePicker={disablePicker}>
                                {selectedLabel}
                            </SelectedLabelText> :
                            <PlaceholderText>
                                {placeholder}
                            </PlaceholderText>
                        }
                    </SelectLabelTouchableOpacity>
                    {!hideDetail ?
                        <>
                            {close === true ?
                                <IconView>
                                    <TouchableOpacity
                                        disabled={disablePicker}
                                        style={{top: 5, right: 10, width: 40, height: 40}}
                                        onPress={() => this.setState({modalVisible: true})}
                                        activeOpacity={1}
                                    >
                                        <Entypo
                                            name="triangle-down"
                                            type="font-awesome"
                                            underlayColor="transparent"
                                            size={18}
                                        />
                                    </TouchableOpacity>

                                    <CloseIconTouchableOpacity
                                        onPress={() => this.props.selectedValue(null)}
                                        activeOpacity={1}
                                        disabled={disablePicker}
                                    >
                                        <Icon
                                            name="close"
                                            type="font-awesome"
                                            underlayColor="transparent"
                                            size={28}
                                        />

                                    </CloseIconTouchableOpacity>
                                </IconView>
                                :
                                <IconView>
                                    <TouchableDropDownIconView
                                        close={close}
                                        disabled={disablePicker}
                                        onPress={() => this.setState({modalVisible: true})}
                                        activeOpacity={1}
                                    >
                                        <Entypo
                                            style={{right: 28}}
                                            name="triangle-down"
                                            type="font-awesome"
                                            underlayColor="transparent"
                                            size={18}
                                        />
                                    </TouchableDropDownIconView>
                                </IconView>
                            }
                        </>
                        : null
                    }
                </SelectView>
                <Modal
                    supportedOrientations={['portrait', 'portrait-upside-down', 'landscape', 'landscape-left', 'landscape-right']}
                    visible={modalVisible}
                    transparent={true}
                    animationType={changeAnimation}
                    onRequestClose={() => this.setState({modalVisible: false, searchText: ''})}
                    style={{height: 90}}
                >
                    <ModalView behavior={Platform.OS === "ios" ? "padding" : null}>
                        <ModalView1>
                            <View style={{flexDirection: "row"}}>
                                <View style={{flex: 1, top: 15, height: 40}}>
                                    {this.props.pickerTitle ? (
                                        <PickerTitleText>
                                            {this.props.pickerTitle}
                                        </PickerTitleText>
                                    ) : null}
                                </View>

                                <TouchableOpacity
                                    style={{right: -5, top: 7, width: 40}}
                                    activeOpacity={0.7}
                                    onPress={() => this.setState({modalVisible: false, searchText: ''})}
                                >
                                    <Icon name='close' size={30}/>
                                </TouchableOpacity>
                            </View>
                            {short !== true ?
                                <View style={styles.searchView}>
                                    <TextInput
                                        onChangeText={text => this._searchFilterFunction(text, dataSource)}
                                        placeholder={searchBarPlaceHolder}
                                        style={styles.textInput}
                                        underlineColorAndroid="transparent"
                                        keyboardType="default"
                                        turnKeyType={"done"}
                                        blurOnSubmit={true}
                                    />
                                </View>
                                :
                                null
                            }
                            <View style={{height: 5}}/>
                            <FlatList
                                keyboardShouldPersistTaps={"always"}
                                nestedScrollEnabled={true}
                                keyExtractor={item => JSON.stringify(item._id)}
                                showsVerticalScrollIndicator={true}
                                refreshing={true}
                                initialNumToRender={100}
                                onEndReachedThreshold={0.5}
                                numColumns={1}
                                data={searchText === '' ? dataSource : dataSourceState}
                                renderItem={({item}) => {
                                    return (
                                        <TouchableItemPickerView
                                            onPress={() => {
                                                selectedValue(item)
                                                this.setState({modalVisible: false, searchText: ''})
                                            }}
                                        >
                                            <ItemPickerView>
                                                {searchText === '' ?
                                                    <Highlighter
                                                        ellipsizeMode="tail"
                                                        searchWords={[searchText]}
                                                        textToHighlight={item.name}
                                                    />
                                                    :
                                                    <Highlighter
                                                        highlightStyle={{fontWeight: 'bold', color: 'black'}}
                                                        searchWords={[searchText]}
                                                        textToHighlight={item.name}
                                                    />
                                                }
                                            </ItemPickerView>
                                        </TouchableItemPickerView>
                                    )
                                }}
                            />
                        </ModalView1>
                    </ModalView>
                </Modal>
                {!hideDetail ?
                    <Line small={small}/>
                    :
                    null
                }
            </View>
        );
    }
}

const styles = StyleSheet.create({
    searchView: {
        top: 5,
        flexDirection: "row",
        height: 40,
        shadowOpacity: 1.0,
        shadowRadius: 5,
        shadowOffset: {
            width: 1,
            height: 1
        },
        backgroundColor: "rgba(255,255,255,1)",
        shadowColor: "#d3d3d3",
        borderRadius: 10,
        elevation: 3,
        marginLeft: 10,
        marginRight: 10,
        marginBottom: 10,
    },
    textInput: {
        color: "black",
        paddingLeft: 15,
        marginTop: Platform.OS === "ios" ? 10 : 0,
        marginBottom: Platform.OS === "ios" ? 10 : 0,
        alignSelf: "center",
        flex: 1,
        textAlign: I18nManager.isRTL ? "right" : "left"
    }
});

const Line = styled(View)`
  top: ${p => p['small'] ? -5 : 10}px;
  width: 100%;
  border-bottom-width: 1px;
  border-bottom-color: #b2b2b2
`;

const SelectView = styled(View)`
  flex-direction: row;
  padding-left: 10px;
`;

const SelectLabelTouchableOpacity = styled(TouchableOpacity)`
  width: ${(p) => p['close'] === true ? 92 : 93}%;
  justify-content: center;
  bottom: 5px;
  flex: 1;
`;

const CloseIconTouchableOpacity = styled(TouchableOpacity)`
  right: 10px;
  width: 40px;
  height: 40px;
`;

const TouchableItemPickerView = styled(TouchableOpacity)`
  justify-content: center;
  flex-direction: row;
  min-height: 40px
`;
const ItemPickerView = styled(View)`
  width: 100%;
  justify-content: center;
  padding-left: 20px;
  padding-right: 10px;
`;

const TouchableDropDownIconView = styled(TouchableOpacity)`
  height: 40px;
`;

const SelectedLabelText = styled(Text)`
  color: ${(p) => p['selectedColorLabel'] ? p['selectedColorLabel'] : (p['disablePicker'] ? '#A9A9A9' : "black")};
  font-size: 16px;
`;

const PlaceholderText = styled(Text)`
  color: #d3d3d3;
  font-size: 16px;
`;

const IconView = styled(View)`
  justify-content: flex-end;
  flex-direction: row;
  left: 15px;
`;

const ModalView = styled(KeyboardAvoidingView)`
  justify-content: center;
  align-items: center;
  background-color: rgba(0, 0, 0, 0.2);
  height: 100%;
`;

const ModalView1 = styled(View)`
  width: 90%;
  max-height: 80%;
  opacity: 10;
  border-radius: 15px;
  background-color: white;
  align-self: center;
  flex-direction: column;
  border-color: black;
`;

const PickerTitleText = styled(Text)`
  font-size: 18px;
  flex: 1;
  color: #000;
  padding-bottom: 10px;
  margin-left: 40px;
  bottom: 5px;
  text-align: center;
`;
