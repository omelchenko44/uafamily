import React, {PureComponent} from 'react';
import styled from 'styled-components';
import DateTimePicker from '@react-native-community/datetimepicker';
import moment from 'moment';
import Touch from '../../../mixin/Touch';
import {DATE_FORMAT} from '../../../config/index';
import {Button, KeyboardAvoidingView, Platform, Text, View,} from 'react-native';
import Modal from "react-native-modal";

export default class DatePicker extends PureComponent {
    state = {
        open: false,
        selectedDate: new Date,
        time: false
    };

    open = () => {
        if (this.props.isVisible === false) {
            this.setState({open: false})
        } else {
            this.setState({open: true});
        }
    };

    close = () => {
        this.setState({open: false, time: true});
    };

    onConfirmDate = (selectedDate) => {
        let {onValue, dateFormat} = this.props;
        this.setState({selectedDate: selectedDate});
        // this.close();
        if (dateFormat === 'DD.MM.YY HH:mm') {
            this.setState({open: false, time: true});
        } else {
            this.setState({open: false});
        }
        onValue(selectedDate)
    };

    onConfirmTime = (selectedDate) => {
        console.log('onConfirmTime')
        this.setState({time: false})
        let {onValue, openCalendar, onConfirmDate} = this.props;
        this.setState({selectedDate: selectedDate});
        onValue(selectedDate)
        if (openCalendar) {
            openCalendar(false)
        }
        onConfirmDate(selectedDate)
    };

    onConfirmDateTimeIOS = (selectedDate) => {
        let {onValue} = this.props;
        this.setState({selectedDate: selectedDate});
        onValue(selectedDate)
    };

    onConfirmDateIOS = (selectedDate) => {
        let {onValue} = this.props;
        this.setState({selectedDate: selectedDate});
        this.setState({open: false});
        onValue(selectedDate)
        this.closeDateIOS()
    };

    closeDateIOS = () => {
        let {openCalendar} = this.props;
        this.setState({open: false})
        if (openCalendar) {
            openCalendar(false)
        }
    };

    render() {
        let {open, time, selectedDate} = this.state;
        let {value, dateFormat, openCalendar, label, showDate, maximumDate} = this.props;
        let resultDateFormat = dateFormat ? dateFormat : DATE_FORMAT
        let date = value && moment(value) || null;
        if (open && openCalendar) {
            openCalendar(true)
        }
        if (Platform.OS === 'android') {
            return (
                <Wrap onPress={() => this.open()}>
                    <Label>{label}</Label>
                    {!showDate ?
                        <Value>{date && date.format(resultDateFormat) || ' '}</Value>
                        : <Value> </Value>
                    }
                    {open &&

                        <DateTimePicker
                            value={date ? date.toDate() : new Date()}
                            maximumDate={maximumDate ? maximumDate : new Date(2300, 10, 20)}
                            display='default'
                            mode="date"
                            onTouchCancel={true}
                            onChange={(event, value) => {
                                if (value) {
                                    this.onConfirmDate(value)
                                } else {
                                    this.setState({open: false})
                                }
                            }}
                        />
                    }
                    {time &&
                        <DateTimePicker
                            value={selectedDate}
                            display='default'
                            mode="time"
                            onChange={(event, value) => {
                                if (value) {
                                    this.onConfirmTime(value)
                                } else {
                                    this.setState({time: false})
                                }
                            }}
                        />
                    }
                </Wrap>
            );
        } else {
            return (
                <View style={{bottom: 10}}>
                    <Wrap1 onPress={this.open}>
                        <Label>{label}</Label>
                        <Text style={{top: 10, fontWeight: "bold"}}>
                            {date && date.format(resultDateFormat) || ' '}
                        </Text>
                    </Wrap1>
                    <Modal
                        supportedOrientations={['portrait', 'portrait-upside-down', 'landscape', 'landscape-left', 'landscape-right']}
                        transparent={true}
                        visible={open}
                        onBackdropPress={() => {
                            this.setState({open: false});
                        }}
                        style={{
                            justifyContent: "flex-end",
                            alignItems: "center",
                            height: Platform.OS === 'ios' ? "95%" : '100%',
                            flex: 0,
                            top: Platform.OS === 'ios' ? 15 : 0
                        }}>
                        <KeyboardAvoidingView behavior={Platform.OS === 'ios' ? "padding" : null}
                                              keyboardVerticalOffset={20}
                                              style={{
                                                  width: '100%',
                                                  alignItems: "center",
                                              }}
                        >
                            {dateFormat === 'DD.MM.YY hh:mm' || dateFormat === 'DD.MM.YY HH:mm' ?
                                <View
                                    style={{backgroundColor: '#DBF8FF', width: '90%', elevation: 10, borderRadius: 10}}>
                                    <DateTimePicker
                                        value={date ? date.toDate() : new Date()}
                                        mode='ime'
                                        display='inline'
                                        pointerEvents="none"
                                        onTouchCancel={true}
                                        onChange={(event, value) => {
                                            this.onConfirmDateTimeIOS(value)
                                        }}
                                    />
                                    <IosView>
                                        <View style={{borderColor: "dodgerblue", borderWidth: 1, borderRadius: 12}}>
                                            <Button onPress={this.closeDateIOS} title={"Зберегти"}/>
                                        </View>
                                    </IosView>
                                </View>
                                :
                                <View
                                    style={{backgroundColor: '#DBF8FF', width: '90%', elevation: 10, borderRadius: 10}}>
                                    <View style={{marginBottom: 60}}>
                                        <DateTimePicker
                                            maximumDate={maximumDate ? maximumDate : new Date(2300, 10, 20)}
                                            value={date ? date.toDate() : new Date()}
                                            mode='date'
                                            display='spinner'
                                            pointerEvents="none"
                                            onTouchCancel={true}
                                            onChange={(event, value) => {
                                                this.onConfirmDateTimeIOS(value);
                                            }}
                                        />
                                    </View>
                                    <IosView>
                                        <View style={{
                                            bottom: 5,
                                            borderColor: "dodgerblue",
                                            borderWidth: 1,
                                            borderRadius: 12
                                        }}>
                                            <Button onPress={this.closeDateIOS} title={"Зберегти"}/>
                                        </View>
                                    </IosView>
                                </View>
                            }
                        </KeyboardAvoidingView>
                    </Modal>
                </View>
            )
        }
    }
}


//region ====================== Styles ========================================
const Wrap = Touch(styled(View)`
  border-bottom-width: 1px;
  height: 40px;
  border-bottom-color: #ccc;
`);

const Wrap1 = Touch(styled.View`
  padding: 10px 10px 15px;
`);

const IosView = styled.View`
  bottom: 45px;
  justify-content: center;
  align-items: center;
  flex-direction: row;
`;

const Value = styled.Text`
  color: #000;
`;
const Label = styled.Text`
  color: ${p => p.focus ? '#1E90FF' : 'gray'};
  font-size: 12px;
`;
