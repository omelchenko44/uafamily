import React, {Component, PureComponent} from 'react';
import styled from 'styled-components';
import {Text, View} from "react-native";
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

export default class Input extends Component {
    static defaultProps = {
        secureTextEntry: false
    };
    state = {
        focus: false,
    };

    onFocus = () => {
        let {onFocus} = this.props;
        this.setState({focus: true});
        if (onFocus) {
            onFocus(true)
        }
    };

    onBlur = () => {
        let {onFocus} = this.props;

        this.setState({focus: false});
        if (onFocus) {
            onFocus(false)
        }
    };

    componentDidMount() {
        if (typeof this.props.ref === "function") {
            this.props.refName(this.refs.inp);
        }
    }

    render() {
        let {focus} = this.state;
        let {value, onValue, label, keyboardType, maxLength, search, textAlign, settings, refName, small} = this.props;
        return (
            <Root focus={focus} settings={settings} small={small}>
                <Label focus={focus}>
                    {label}
                </Label>

                <TextInput
                    ref={(ref) => {
                        if (refName) {
                            refName(ref)
                        }
                    }}
                    small={small}
                    settings={settings}
                    secureTextEntry={this.props.secureTextEntry}
                    value={`${value}`}
                    keyboardType={keyboardType}
                    onChangeText={onValue}
                    onFocus={this.onFocus}
                    onBlur={this.onBlur}
                    textAlign={textAlign ? textAlign : 'left'}
                    maxLength={maxLength}
                    returnKeyType='done'
                />
                {search &&
                    <View focus={focus} style={{flexDirection: "row", justifyContent: "flex-end", width: '100%'}}>
                        <Icon name={'folder-search'} style={{bottom: 30}} size={20}/>
                    </View>
                }

            </Root>
        );
    }
}

export class Label extends PureComponent {
    render() {
        let {children} = this.props;
        return (
            <LabelBlock focus={this.props.focus}>
                {children}
            </LabelBlock>
        );
    }
}

//region ====================== Styles ========================================
export const Root = styled(View)`
  border-bottom-width: 1px;
  height: ${p => p['settings'] === true ? 35 : (p['small'] === true ? 55 : 60)}px;
  border-bottom-color: ${p => p.focus ? '#1E90FF' : 'gray'};
`;

const TextInput = styled.TextInput`
  padding: ${p => p['settings'] ? 15 : (p['small'] === true ? 30 : 30)}px 10px 5px;
`;

const LabelBlock = styled(Text)`
  padding-top: 7px;
  position: absolute;
  color: ${p => p['focus'] ? '#1E90FF' : 'gray'};
  font-size: 12px;
`;
//endregion
