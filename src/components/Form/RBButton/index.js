import React, {Component} from 'react';
import {Text, TouchableOpacity} from 'react-native';
import styled from 'styled-components';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

export default class RBButton extends Component {

    state = {};

    render() {
        let {onClick, iconName} = this.props;
        if (this.props.isCamera === true) {
            return (
                <RootGrey>
                    <TouchableOpacity onPress={() => onClick()}>
                        <Icon style={{top: 5, left: 5}} name={iconName} size={35}/>
                    </TouchableOpacity>
                </RootGrey>
            );
        } else {
            return (
                <Root>
                    <TouchableOpacity onPress={() => onClick()}>
                        <Text style={{fontSize: 17}}>Додати людину</Text>
                    </TouchableOpacity>
                </Root>
            );
        }
    }
}

//region ====================== Styles ========================================

export const Root = styled.View`
  position: absolute;
  bottom: 25px;
  right: 15px;
  padding: 10px;
  border-radius: 12px;
  background-color: #32c096;
  border-width: 1px;
  border-color: gray;
`;
export const RootGrey = styled.View`
  position: absolute;
  bottom: ${Platform.select({ios: 0, android: 6})}px;
  right: ${Platform.select({ios: -5, android: 0})}px;
  width: 48px;
  height: 48px;
  border-radius: 12px;
  background-color: #d3d3d3;
  border-width: 1px;
`;

//endregion
