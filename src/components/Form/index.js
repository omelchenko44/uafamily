import React, {Component} from 'react';
import PureInput from './Input';
import PureDatePicker from './DatePicker';
import PureRBButton from './RBButton';
import SRCHSelect from './Select/searchableSelect';

const {Provider, Consumer} = React.createContext(null);

export default class Form extends Component {
    constructor() {
        super();
        this.state = this.state || {};
        this.state.values = this.state.values || {};
    }

    getValue(name) {
        return this.state.values[name];
    }

    setValue(name, value) {
        this.state.values[name] = value;
        this.forceUpdate();
    }

    provide(children) {
        return (
            <Provider value={this}>
                {children}
            </Provider>
        );
    }
}

function Wrapper(Class) {
    return class extends Component {
        render() {
            return (
                <Consumer>
                    {form => {
                        if (!form) {
                            return <Class {...this.props}/>;
                        }

                        if (!this.onValue) {
                            this.onValue = value => {
                                form.setValue(this.props.name, value);
                            };
                        }

                        return (
                            <Class
                                {...this.props}
                                value={form.getValue(this.props.name)}
                                onValue={this.onValue}
                            />
                        );
                    }}
                </Consumer>
            );
        }
    };
}

export const Input = Wrapper(PureInput);
export const DatePicker = Wrapper(PureDatePicker);
export const RBButton = PureRBButton;
export const SearchableSelect = Wrapper(SRCHSelect);
