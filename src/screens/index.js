import React from "react";
import Main from "./Main";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import AddNewPeopleList from "./AddNewPeoplesList";
import Profile from "./Profile";
import {createBottomTabNavigator} from "@react-navigation/bottom-tabs";
import Foundation from "react-native-vector-icons/Foundation";

const Tab = createBottomTabNavigator();

export default function BottomTabs() {
    return (
        <Tab.Navigator initialRouteName="Main"
                       screenOptions={() => ({
                           tabBarInactiveTintColor: 'white',
                           tabBarActiveTintColor: '#f9d548',
                           headerShown: false,
                           tabBarStyle: {backgroundColor: "#6495EDFF"}

                       })}>
            <Tab.Screen name={'Main'}
                        component={Main}
                        options={{
                            tabBarIcon: ({color}) => (
                                <FontAwesome name={"search"} color={color} size={30} style={{top: 10}}/>
                            ),
                            tabBarLabel: '',
                            headerShow: false
                        }}/>
            <Tab.Screen name={'AddNewPeoplesList'}
                        component={AddNewPeopleList}
                        options={{
                            tabBarIcon: ({color}) => (
                                <Foundation name={"plus"} color={color} size={30} style={{top: 10}}/>
                            ),
                            tabBarLabel: ''
                        }}/>
            <Tab.Screen name={'Profile'}
                        component={Profile}
                        options={{
                            tabBarIcon: ({color}) => (
                                <FontAwesome name={"user"} color={color} size={30} style={{top: 10}}/>
                            ),
                            tabBarLabel: ''
                        }}/>
        </Tab.Navigator>
    )
}
