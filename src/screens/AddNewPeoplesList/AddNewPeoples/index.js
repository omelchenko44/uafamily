import React, {PureComponent} from "react";
import {
    ActivityIndicator, Dimensions,
    Image,
    Keyboard,
    SafeAreaView,
    ScrollView,
    Text,
    TextInput,
    TouchableOpacity,
    TouchableWithoutFeedback,
    View
} from "react-native";
import {DatePicker, Input} from "../../../components/Form";
import TopMenu from "../../../components/navidation/topMenu";
import {launchImageLibrary} from 'react-native-image-picker';
import AntDesign from "react-native-vector-icons/AntDesign";
import Lightbox from "react-native-lightbox-v2";
import {firebase} from "@react-native-firebase/auth";
import firestore from "@react-native-firebase/firestore";
import clog from "../../../utils/clog";
import storage from '@react-native-firebase/storage';
import Modal from "react-native-modal";
import MyAwesomeAlert from "../../../components/Form/AwesomeAlert";

export default class AddNewPeoples extends PureComponent {
    constructor(props) {
        super(props);
        let {updateFunction, user} = props.route.params;
        this.state = {
            name: user ? user.name : '',
            surname: user ? user.surname : '',
            patronymic: user ? user.patronymic : '',
            birthday: user ? new Date(user.birthday) : new Date().getTime(),
            last_number: user ? user.last_number : '',
            email: user ? user.email : '',
            comment: user ? user.comment : ' ',
            photoURL: user ? user.photoURL : '',
            updateFunction: updateFunction,
            navigation: props.navigation,
            focus: false,
            showLoader: false,
        }
    }

    componentDidMount() {
        let {navigation} = this.state;
        navigation.addListener('beforeRemove', (e) => {
            this.setState({
                showAlert: true,
                title: "Скасувати зміни?",
                errorMessage: "У вас є не збережені зміни. Ви впевнені що не хочете зберегти їх?",
                discard: e.data.action,
                showConfirmButton: true,
                cancelText: "Відмінити",
                confirmText: "Не зберігати",
            })
            e.preventDefault();
        })
    }

    hideAlert = () => {
        this.setState({
            showAlert: false
        });
    };

    showAlert = (message) => {
        this.setState({
            loadData: false,
            showAlert: true,
            title: 'Помилка!',
            titleStyle: {color: "red"},
            errorMessage: message,
            showConfirmButton: false,
            cancelText: "Закрити",
        })
    };

    addPhoto(photo) {
        this.setState({photoURL: photo[0].uri, random: Math.random()});
    }

    removePhoto() {
        this.setState({photoURL: '', random: Math.random()});
    }

    newId() {
        return firebase.firestore().collection('peoples').doc().id;
    }

    async saveAllData() {
        let {
            name,
            surname,
            patronymic,
            birthday,
            last_number,
            comment,
            email,
            photoURL,
            updateFunction,
            navigation
        } = this.state;
        if (name === "") {
            this.showAlert('Введіть імя людини');
            return;
        }
        if (surname === "") {
            this.showAlert('Введіть прізвище людини');
            return;
        }
        if (patronymic === "") {
            this.showAlert('Введіть по-батькові людини');
            return;
        }
        if (photoURL === "") {
            this.showAlert('Додайте фото людини');
            return;
        }
        this.setState({showLoader: true});

        let user = firebase.auth().currentUser;
        let photoURLFirebase = await this.uploadPhoto(photoURL, user.uid);
        let newUserUid = this.newId();
        clog(newUserUid)
        let newUserObject = {
            newUserUid: newUserUid,
            name: name,
            surname: surname,
            patronymic: patronymic,
            birthday: birthday,
            last_number: last_number,
            comment: comment,
            email: email,
            authorPost: user.email,
            photoURL: photoURLFirebase,
        };
        firestore()
            .collection('peoples')
            .doc(newUserUid)
            .set(newUserObject)
            .then(() => {
                this.setState({showLoader: false});
                console.log('User data updated!');
                if (updateFunction) {
                    updateFunction(newUserObject);
                }
                setTimeout(()=>{
                    navigation.removeListener('beforeRemove')
                    navigation.goBack();
                }, 100);
            })
            .catch((e) => {
                clog(e)
                this.setState({showLoader: false});
            });

        //додати поле з кількістю тих хто нажав що людина знайдена;
    }

    async uploadPhoto(fileUri, id) {
        const fileName = fileUri?.split('/').pop();
        const storageRef = storage().ref(`${id}/${fileName}`);
        const task = storageRef.putFile(fileUri)
        task.on('state_changed', taskSnapshot => {
            console.log(`${taskSnapshot.bytesTransferred} transferred out of ${taskSnapshot.totalBytes}`);
        });
        task.catch((e) => {
            clog(e)
        })
        return task.then(async () => {
            let imageRef = storage().ref(`${id}/${fileName}`);
            return await imageRef.getDownloadURL();
        });
    }

    render() {
        let {
            name,
            surname,
            patronymic,
            birthday,
            last_number,
            comment,
            photoURL,
            email,
            focus,
            showLoader,
            showAlert,
            title,
            errorMessage,
            showConfirmButton,
            cancelText,
            confirmText,
            discard
        } = this.state;
        let {navigation} = this.props;
        return (
            <SafeAreaView style={{backgroundColor: '#c4e4ef'}}>
                <TopMenu
                    navigationBack={navigation}
                />
                <TouchableWithoutFeedback
                    accessible={false}
                    onPress={() => {
                        Keyboard.dismiss();
                    }}>
                    <View>
                        <ScrollView style={{margin: 10, height: '100%'}}>
                            <View>
                                <Input
                                    keyboardType="default"
                                    name="name"
                                    label={"Ім'я"}
                                    value={name}
                                    onValue={(name) => {
                                        this.setState({name});
                                    }}
                                />
                            </View>
                            <View>
                                <Input
                                    keyboardType="default"
                                    name="surname"
                                    label={"Прізвище"}
                                    value={surname}
                                    onValue={(surname) => {
                                        this.setState({surname});
                                    }}
                                />
                            </View>
                            <View>
                                <Input
                                    keyboardType="default"
                                    name="patronymic"
                                    label={"По-батькові"}
                                    value={patronymic}
                                    onValue={(patronymic) => {
                                        this.setState({patronymic});
                                    }}
                                />
                            </View>
                            <View style={{marginTop: 10}}>
                                <DatePicker
                                    name="date"
                                    label={'Дата народження'}
                                    value={birthday}
                                    onValue={(birthday) => {
                                        this.setState({birthday});
                                    }}
                                    dateFormat={'DD.MM.YYYY'}
                                />
                            </View>
                            <View>
                                <Input
                                    keyboardType="default"
                                    name="last_number"
                                    label={"Контакти"}
                                    value={last_number}
                                    onValue={(last_number) => {
                                        this.setState({last_number});
                                    }}
                                />
                            </View>
                            <View>
                                <Input
                                    keyboardType="email-address"
                                    name="email"
                                    label={"Електронна пошта"}
                                    value={email}
                                    onValue={(email) => {
                                        this.setState({email});
                                    }}
                                />
                            </View>
                            <View style={{marginTop: 10}}>
                                <Text style={{color: focus ? 'dodgerblue' : 'gray'}}>Коментар або додаткова
                                    інформація:</Text>
                                <View style={{
                                    borderWidth: 1,
                                    marginTop: 10,
                                    borderColor: '#ACACAC',
                                    justifyContent: 'flex-end',
                                    flexDirection: 'column'
                                }}>
                                    <TextInput
                                        keyboardType="default"
                                        value={comment}
                                        maxLength={1000}
                                        multiline={true}
                                        numberOfLines={6}
                                        style={{
                                            color: 'gray',
                                            height: 140,
                                            textAlignVertical: 'top'
                                        }}
                                        onFocus={() => {
                                            this.setState({focus: true})
                                        }}
                                        onBlur={() => {
                                            this.setState({focus: false})
                                        }}
                                        onChangeText={(comment) => {
                                            this.setState({comment});
                                        }}
                                    />
                                </View>
                            </View>
                            <ScrollView style={{flexDirection: 'row', marginTop: 25}}
                                        horizontal={true}>
                                <TouchableOpacity
                                    style={{
                                        borderRadius: 5,
                                        borderWidth: 1,
                                        marginLeft: 10,
                                        borderColor: '#ACACAC',
                                        padding: 50
                                    }}
                                    onPress={async () => {
                                        let options = {mediaType: 'photo', selectionLimit: 10}
                                        // @ts-ignore
                                        const result = await launchImageLibrary(options);
                                        if (result.didCancel !== true) {
                                            // @ts-ignore
                                            this.addPhoto(result.assets);
                                        }
                                    }}
                                >
                                    <AntDesign name={"pluscircleo"} size={28}/>
                                </TouchableOpacity>
                                {photoURL !== "" &&
                                    <>
                                        <View style={{marginLeft: 10}}>
                                            <Lightbox
                                                renderContent={() => (
                                                      <View style={{}}>
                                                        <Image
                                                            style={{flex: 1}}
                                                            source={{uri: photoURL}}
                                                            resizeMode={"contain"}
                                                        />
                                                    </View>
                                                )}>
                                                <View style={{
                                                    flexDirection: 'row',
                                                    bottom: 6
                                                }}>
                                                    <Image
                                                        style={{height: 130, width: 130}}
                                                        source={{uri: photoURL}}
                                                        resizeMode={"stretch"}
                                                    />
                                                    <TouchableOpacity
                                                        style={{position: 'absolute', right: 0, top: 10}}
                                                        onPress={() => {
                                                            this.removePhoto(photoURL);
                                                        }}
                                                    >
                                                        <AntDesign name={'closecircleo'}
                                                                   size={20}
                                                                   color={'red'}/>
                                                    </TouchableOpacity>
                                                </View>
                                            </Lightbox>
                                        </View>
                                    </>
                                }
                            </ScrollView>
                            <View style={{width: "100%", alignItems: 'center', marginTop: 20}}>
                                <TouchableOpacity
                                    style={{
                                        width: '90%',
                                        backgroundColor: 'dodgerblue',
                                        height: 40,
                                        justifyContent: "center",
                                        alignItems: 'center',
                                        borderRadius: 5
                                    }}
                                    onPress={async () => {
                                        await this.saveAllData();
                                    }}>
                                    <Text style={{fontSize: 17, fontWeight: 'bold', color: 'white'}}>Зберегти</Text>
                                </TouchableOpacity>
                            </View>
                        </ScrollView>
                    </View>
                </TouchableWithoutFeedback>
                <Modal
                    supportedOrientations={['portrait', 'portrait-upside-down', 'landscape', 'landscape-left', 'landscape-right']}
                    animationType={"slide"}
                    isVisible={showLoader}
                    backdropTransitionOutTiming={0}>
                    <View style={{
                        width: Dimensions.get('window').width,
                        height: Dimensions.get('window').height,
                        right: 22,
                        backgroundColor: 'rgba(149,149,149,0.27)',
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}>
                        <ActivityIndicator size="large" color={'dodgerblue'}/>
                    </View>
                </Modal>
                <MyAwesomeAlert
                    showAlert={showAlert}
                    title={title}
                    errorMessage={errorMessage}
                    showConfirmButton={showConfirmButton}
                    cancelText={cancelText}
                    confirmText={confirmText}
                    hideAlert={this.hideAlert}
                    navigation={navigation}
                    discard={discard}/>
            </SafeAreaView>
        )
    }
}
