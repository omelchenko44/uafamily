import React, {useEffect, useState} from "react";
import {Dimensions, Image, SafeAreaView, ScrollView, Text, TouchableOpacity, View} from "react-native";
import TopMenu from "../../components/navidation/topMenu";
import RBButton from "../../components/Form/RBButton";
import {dateFormat} from "../../utils/dateFormat";
import MyAwesomeAlert from "../../components/Form/AwesomeAlert";
import firestore from "@react-native-firebase/firestore";
import {firebase} from "@react-native-firebase/auth";
import clog from "../../utils/clog";

export default function AddNewPeopleList(props) {
    let {navigation} = props;
    const [userList, setUserList] = useState([]);
    const [showAlert, setShowAlert] = useState(false);
    const [errorMessage, setErrorMessage] = useState("");
    const [selectedUser, setSelectedUser] = useState("");
    const [showUserList, setShowUserList] = useState(true);

    function goToAddNewPeople(user) {
        navigation.navigate("AddNewPeoples", {updateFunction: getAddedPeoples, user: user ? user : null});
    }

    useEffect(() => {
        getAddedPeoples().then(() => {
        });
    }, [])

    async function getAddedPeoples() {
        let showUserList = [];
        let user = firebase.auth().currentUser;
        const peoplesDocument = await firestore().collection('peoples')
            .where("authorPost", "==", user.email)
            .get();
        setShowUserList(false);
        peoplesDocument.docs.map((user) => {
            showUserList.push(user.data());
            clog(user.data())
        })
        setUserList(showUserList);
        setTimeout(() => {
            setShowUserList(true);
        }, 10);
    }

    const showAlertFunction = (message, user) => {
        setErrorMessage(message);
        setShowAlert(true);
        setSelectedUser(user)
    };

    const hideAlert = () => {
        setShowAlert(false);
    }

    const deleteUserData = () => {
        let newUserList = [];
        for (let i = 0; i < userList.length; i++) {
            if (selectedUser.name !== userList[i].name && selectedUser.surname !== userList[i].surname) {
                newUserList.push(userList[i]);
            }
        }
        setUserList(newUserList);
        setShowAlert(false);
    }

    return (
        <SafeAreaView style={{backgroundColor: '#c4e4ef'}}>
            <TopMenu/>
            <View>
                {showUserList &&
                    <ScrollView style={{padding: 5, flexDirection: 'column', height: '100%'}}>
                        {userList.map((user) => (
                            <View
                                key={user.newUserUid}
                                style={{
                                    backgroundColor: 'white',
                                    padding: 10,
                                    borderRadius: 10,
                                    shadowColor: 'black',
                                    shadowOpacity: 0.2,
                                    shadowOffset: {width: 4, height: 10},
                                    shadowRadius: 3,
                                    elevation: 5,
                                    marginBottom: 20
                                }}>
                                {clog(user.photoURL)}
                                {user.photoURL ?
                                    <View style={{
                                        justifyContent: "center",
                                        alignItems: 'center'
                                    }}>
                                        <Image
                                            style={{
                                                height: 300,
                                                width: "100%",
                                                borderRadius: 10
                                            }}
                                            source={{uri: user.photoURL}}
                                            resizeMode={"contain"}
                                        />
                                    </View>
                                    :
                                    <View style={{
                                        height: 200,
                                        width: '100%',
                                        backgroundColor: 'whitesmoke',
                                        borderRadius: 10,
                                        justifyContent: "center",
                                        alignItems: 'center'
                                    }}>
                                        <Image
                                            style={{
                                                height: 150,
                                                width: Dimensions.get("window").width * 0.4,
                                            }}
                                            source={require('../../assets/noPhotoIcon.png')}
                                            resizeMode={"stretch"}
                                        />
                                    </View>
                                }
                                <View style={{flexDirection: 'row', marginTop: 0}}>
                                    <View style={{width: "50%"}}>
                                        <Text style={{fontWeight: 'bold', fontSize: 17}}>Ім'я:</Text>
                                    </View>
                                    <Text style={{fontSize: 17}}>{user.name}</Text>
                                </View>
                                <View style={{flexDirection: 'row', marginTop: 10}}>
                                    <View style={{width: "50%"}}>
                                        <Text style={{fontWeight: 'bold', fontSize: 17}}>Прізвище:</Text>
                                    </View>
                                    <Text style={{fontSize: 17}}>{user.surname}</Text>
                                </View>
                                <View style={{flexDirection: 'row', marginTop: 10}}>
                                    <View style={{width: "50%"}}>
                                        <Text style={{fontWeight: 'bold', fontSize: 17}}>Дата народження:</Text>
                                    </View>
                                    <Text style={{fontSize: 17}}>{dateFormat(user.birthday, "DD.MM.YYYY")}</Text>
                                </View>
                                <View style={{justifyContent: 'center', alignItems: 'center', marginTop: 15}}>
                                    <TouchableOpacity
                                        style={{
                                            backgroundColor: '#fca41c',
                                            height: 40,
                                            borderRadius: 10,
                                            justifyContent: 'center',
                                            alignItems: 'center',
                                            width: '100%'
                                        }}
                                        onPress={() => {
                                            goToAddNewPeople(user);
                                        }}>
                                        <Text style={{fontSize: 17, color: 'white', fontWeight: 'bold'}}>Більше
                                            інформації</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={{justifyContent: 'center', alignItems: 'center', marginTop: 15}}>
                                    <TouchableOpacity
                                        style={{
                                            backgroundColor: '#3CB371',
                                            height: 40,
                                            borderRadius: 10,
                                            justifyContent: 'center',
                                            alignItems: 'center',
                                            width: '100%'
                                        }}
                                        onPress={() => {
                                            showAlertFunction("Ви впевнені що знайшли користувача і хочете видалити дані про нього", user)
                                        }}>
                                        <Text style={{fontSize: 17, color: 'white', fontWeight: 'bold'}}>Я знайшов цю
                                            людину</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        ))}
                        <View style={{height: 150}}/>
                    </ScrollView>
                }
                <View style={{bottom: 90}}>
                    <RBButton onClick={goToAddNewPeople}
                              iconName={"plus"}/>
                </View>
            </View>
            <MyAwesomeAlert
                showAlert={showAlert}
                title={'Попередження'}
                errorMessage={errorMessage}
                showConfirmButton={true}
                cancelText={'Закрити'}
                confirmText={'Видалити'}
                customFunction={deleteUserData}
                hideAlert={hideAlert}
            />
        </SafeAreaView>
    )
}
