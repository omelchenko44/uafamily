import React, {useContext, useState} from "react";
import {
    Image,
    KeyboardAvoidingView,
    Platform,
    SafeAreaView,
    StyleSheet,
    Text,
    TextInput,
    TouchableOpacity,
    View,
} from "react-native";
import MyAwesomeAlert from "../../components/Form/AwesomeAlert";
import {AuthContext} from "../../firebaseAuth/AuthProvider";
import Ionicons from "react-native-vector-icons/Ionicons";
import clog from "../../utils/clog";

export const CreateAccount = ({navigation}) => {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [passwordConf, setPasswordConf] = useState("");
    const [showAlert, setShowAlert] = useState(false);
    const [errorMessage, setErrorMessage] = useState('');
    const [passwordClosed, setPasswordClosed] = useState(true);
    const [passwordConfClosed, setPasswordConfClosed] = useState(true);
    const [iconName, setIconName] = useState('eye-off');
    const [iconConfName, setIconConfName] = useState('eye-off');

    const {register} = useContext(AuthContext);

    function registerAccount() {
        if (email === '') {
            showingAlert("Будь-ласка введіть email");
            return;
        }
        if (validate(email) === false) {
            showingAlert("Будь-ласка введіть правильний email");
            return;
        }
        if (password === '') {
            showingAlert("Будь-ласка введіть пароль");
            return;
        }
        if (password !== passwordConf) {
            showingAlert("Паролі не співпадають");
            return;
        }
        try {
            register(email.toString().trim(), password, showingAlert);
        } catch (e) {
            clog(e)
            clog('e')
        }
    }

    function validate(email) {
        let reg = /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w\w+)+$/;
        return reg.test(email)
    }

    function goToSignInScreen() {
        navigation.goBack();
    }

    function hideAlert() {
        setShowAlert(!showAlert)
    }

    function showingAlert(message) {
        setShowAlert(true);
        setErrorMessage(message);
    }

    function changePasswordIcon(type, state) {
        if (type === 'password') {
            setIconName(state === true ? 'eye-off' : 'eye');
            setPasswordClosed(state);
        } else {
            setIconConfName(state === true ? 'eye-off' : 'eye');
            setPasswordConfClosed(state);
        }
    }

    return (
        <SafeAreaView style={{justifyContent: 'space-between', flex: 1, backgroundColor: '#c4e4ef'}}>
            <View>
                <View style={{
                    alignItems: "center",
                    marginTop: 72,
                    flexDirection: 'row',
                    justifyContent: 'center',
                    width: "100%"
                }}>
                    <Image
                        source={require("../../assets/logo.png")}
                        style={{height: 30, width: 30}}/>
                    <Text style={{
                        fontSize: 20,
                        marginLeft: 5,
                        marginBottom: 3,
                        fontWeight: "bold",
                        color: 'black'
                    }}>
                        <Text style={{color: '#2b75e5'}}>U</Text>
                        <Text style={{color: '#f9d548'}}>A</Text> Family
                    </Text>
                </View>
                <View style={{alignItems: "center", marginTop: 50}}>
                    <Text style={{fontSize: 24}}>Створити аккаунт</Text>
                </View>
            </View>

            <KeyboardAvoidingView
                behavior={Platform.OS === "ios" ? "padding" : null}
                keyboardVerticalOffset={Platform.OS === "ios" ? 0 : 0}
                style={{}}>
                <View style={{marginLeft: 24, width: '100%'}}>
                    <TextInput
                        value={email}
                        onChangeText={(email) => setEmail(email)}
                        placeholder={"Email"}
                        placeholderTextColor={'gray'}
                        style={styles.input}
                        keyboardType={'email-address'}
                    />
                </View>
                <View style={{flexDirection: "row"}}>
                    <View style={{marginLeft: 24, width: '100%'}}>
                        <TextInput
                            value={password}
                            onChangeText={(password) => setPassword(password)}
                            placeholder={"Пароль"}
                            placeholderTextColor={'gray'}
                            style={styles.input}
                            secureTextEntry={passwordClosed}
                        />
                        <TouchableOpacity
                            style={{position: 'absolute', right: 40, top: 10}}
                            onPress={() => {
                                changePasswordIcon('password', !passwordClosed)
                            }}>
                            <Ionicons name={iconName} size={25}/>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{flexDirection: "row"}}>
                    <View style={{marginLeft: 24, width: '100%'}}>
                        <TextInput
                            value={passwordConf}
                            onChangeText={(passwordConf) => setPasswordConf(passwordConf)}
                            placeholder={"Повторіть пароль"}
                            placeholderTextColor={'gray'}
                            style={styles.input}
                            secureTextEntry={passwordConfClosed}
                        />
                        <TouchableOpacity
                            style={{position: 'absolute', right: 40, top: 10}}
                            onPress={() => {
                                changePasswordIcon('passwordConf', !passwordConfClosed)
                            }}>
                            <Ionicons name={iconConfName} size={25}/>
                        </TouchableOpacity>
                    </View>
                </View>
            </KeyboardAvoidingView>

            <View style={{width: '100%', justifyContent: "center", alignItems: "center"}}>
                <TouchableOpacity
                    onPress={() => {
                        registerAccount();
                    }}
                    style={{
                        width: "86%",
                        backgroundColor: 'dodgerblue',
                        marginRight: 24,
                        marginLeft: 24,
                        justifyContent: "center",
                        alignItems: "center",
                        height: 50,
                        borderRadius: 4,
                        marginBottom: 20
                    }}>
                    <Text style={{fontSize: 17, color: 'white'}}>Створити аккаунт</Text>
                </TouchableOpacity>
                <View style={{marginBottom: 20, alignItems: "center", justifyContent: "center", flexDirection: "row"}}>
                    <Text style={{fontSize: 15}}>Уже є аккаунт?</Text>
                    <TouchableOpacity onPress={() => {
                        goToSignInScreen();
                    }}>
                        <Text style={{color: 'dodgerblue'}}> Ввійти</Text>
                    </TouchableOpacity>
                </View>
            </View>
            <MyAwesomeAlert showAlert={showAlert}
                            title={"Попередження"}
                            errorMessage={errorMessage}
                            showConfirmButton={false}
                            cancelText={"Відмінити"}
                            hideAlert={hideAlert}
                            navigation={navigation}
                            titleStyle={{color: '#ffba00'}}
            />
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    input: {
        paddingTop: 10,
        marginRight: 34,
        paddingBottom: 10,
        borderBottomWidth: 1,
        borderColor: "silver",
        marginBottom: 30,
        borderRadius: 7,
        flexDirection: "row",
        alignItems: "center"
    },
});

