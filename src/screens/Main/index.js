import React, {useState} from "react";
import {Dimensions, Image, SafeAreaView, ScrollView, Text, TouchableOpacity, View} from "react-native";
import TopMenu from "../../components/navidation/topMenu";
import {Input} from "../../components/Form";
import firestore from "@react-native-firebase/firestore";
import MyAwesomeAlert from "../../components/Form/AwesomeAlert";
import {dateFormat} from "../../utils/dateFormat";

export default function Main(props) {
    let {navigation} = props;
    const [name, setName] = useState("");
    const [surname, setSurname] = useState("");
    const [patronymic, setPatronymic] = useState("");
    const [showAlert, setShowAlert] = useState(false);
    const [errorMessage, setErrorMessage] = useState("");
    const [allDataEntered, setAllDataEntered] = useState(false);
    const [findUserList, setFindUserList] = useState([]);
    const [showFindUserList, setShowFindUserList] = useState(true);

    const showAlertFunction = (message) => {
        setErrorMessage(message);
        setShowAlert(true);
    };

    const hideAlert = () => {
        setShowAlert(false);
    }

    async function getPeoples() {
        let findUserList = [];
        const peoplesDocument = await firestore().collection('peoples')
            .where("name", "==", name)
            .where("surname", "==", surname)
            .where("patronymic", "==", patronymic)
            .get();
        if (peoplesDocument.docs.length === 0) {
            showAlertFunction('Перевірьте правильність введених данних або такого користувача не існує');
        }
        setShowFindUserList(false);
        peoplesDocument.docs.map((user) => {
            findUserList.push(user.data());
        })
        setFindUserList(findUserList);
        setTimeout(() => {
            setShowFindUserList(true);
        }, 10);
    }

    function checkAllDataEntered(name, surname, patronymic) {
        if (name !== '' && surname !== '' && patronymic !== '') {
            setAllDataEntered(true);
        } else {
            setAllDataEntered(false);
        }
    }

    function removeSearchData() {
        setName("");
        setSurname("");
        setPatronymic("");
        setShowAlert(false);
        setErrorMessage("");
        setAllDataEntered(false);
        setFindUserList([]);
    }

    return (
        <SafeAreaView style={{backgroundColor: '#c4e4ef'}}>
            <TopMenu navigation={navigation}/>
            <View style={{padding: 10, flexDirection: 'column', height: '100%'}}>
                <ScrollView>
                    <View>
                        <Text style={{fontSize: 17, fontWeight: 'bold'}}>Шукаю:</Text>
                    </View>
                    <View>
                        <Input
                            keyboardType="default"
                            name="surname"
                            label={"Прізвище"}
                            value={surname}
                            onValue={(surname) => {
                                setSurname(surname);
                                checkAllDataEntered(name, surname, patronymic);
                            }}
                        />
                    </View>
                    <View>
                        <Input
                            keyboardType="default"
                            name="name"
                            label={"Ім'я"}
                            value={name}
                            onValue={(name) => {
                                setName(name);
                                checkAllDataEntered(name, surname, patronymic);
                            }}
                        />
                    </View>
                    <View>
                        <Input
                            keyboardType="default"
                            name="patronymic"
                            label={"По-батькові"}
                            value={patronymic}
                            onValue={(patronymic) => {
                                setPatronymic(patronymic);
                                checkAllDataEntered(name, surname, patronymic);
                            }}
                        />
                    </View>
                    <View style={{alignItems: 'center'}}>
                        <TouchableOpacity
                            style={{
                                width: '90%',
                                height: 40,
                                backgroundColor: allDataEntered ? 'dodgerblue' : 'gray',
                                marginTop: 20,
                                marginBottom: 10,
                                justifyContent: 'center',
                                alignItems: 'center',
                                borderRadius: 5
                            }}
                            disabled={!allDataEntered}
                            onPress={async () => {
                                await getPeoples();
                            }}>
                            <Text style={{fontSize: 17, color: 'white', fontWeight: 'bold'}}>Знайти</Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            style={{
                                width: '90%',
                                height: 40,
                                borderColor: 'gray',
                                borderWidth: 1,
                                marginBottom: 30,
                                justifyContent: 'center',
                                alignItems: 'center',
                                borderRadius: 5
                            }}
                            onPress={() => {
                                removeSearchData();
                            }}>
                            <Text style={{fontSize: 17, color: 'black', fontWeight: 'bold'}}>Очистити дані
                                пошуку</Text>
                        </TouchableOpacity>
                    </View>
                    {showFindUserList &&
                        <>
                            {
                                findUserList.map((user) => (
                                    <View style={{
                                        backgroundColor: 'white',
                                        padding: 10,
                                        borderRadius: 10,
                                        shadowColor: 'black',
                                        shadowOpacity: 0.2,
                                        shadowOffset: {width: 4, height: 10},
                                        shadowRadius: 3,
                                        elevation: 5,
                                        marginBottom: 20
                                    }}>
                                        {user.photo !== '' ?
                                            <View style={{
                                                justifyContent: "center",
                                                alignItems: 'center'
                                            }}>
                                                <Image
                                                    style={{
                                                        height: 300,
                                                        width: "100%",
                                                        borderRadius: 10
                                                    }}
                                                    source={{uri: user.photo}}
                                                    resizeMode={"contain"}
                                                />
                                            </View>
                                            :
                                            <View style={{
                                                height: 200,
                                                width: '100%',
                                                backgroundColor: 'whitesmoke',
                                                borderRadius: 10,
                                                justifyContent: "center",
                                                alignItems: 'center'
                                            }}>
                                                <Image
                                                    style={{
                                                        height: 150,
                                                        width: Dimensions.get("window").width * 0.4,
                                                    }}
                                                    source={require('../../assets/noPhotoIcon.png')}
                                                    resizeMode={"stretch"}
                                                />
                                            </View>
                                        }
                                        <View style={{flexDirection: 'row', marginTop: 10}}>
                                            <View style={{width: "50%"}}>
                                                <Text style={{fontWeight: 'bold', fontSize: 17}}>Прізвище:</Text>
                                            </View>
                                            <Text style={{fontSize: 17}}>{user.surname}</Text>
                                        </View>
                                        <View style={{flexDirection: 'row', marginTop: 0}}>
                                            <View style={{width: "50%"}}>
                                                <Text style={{fontWeight: 'bold', fontSize: 17}}>Ім'я:</Text>
                                            </View>
                                            <Text style={{fontSize: 17}}>{user.name}</Text>
                                        </View>
                                        <View style={{flexDirection: 'row', marginTop: 0}}>
                                            <View style={{width: "50%"}}>
                                                <Text style={{fontWeight: 'bold', fontSize: 17}}>По-батькові:</Text>
                                            </View>
                                            <Text style={{fontSize: 17}}>{user.patronymic}</Text>
                                        </View>
                                        <View style={{flexDirection: 'row', marginTop: 10}}>
                                            <View style={{width: "50%"}}>
                                                <Text style={{fontWeight: 'bold', fontSize: 17}}>Дата
                                                    народження:</Text>
                                            </View>
                                            <Text
                                                style={{fontSize: 17}}>{dateFormat(user.birthday, "DD.MM.YYYY")}</Text>
                                        </View>
                                        <View style={{flexDirection: 'row', marginTop: 0}}>
                                            <View style={{width: "50%"}}>
                                                <Text style={{fontWeight: 'bold', fontSize: 17}}>E-mail:</Text>
                                            </View>
                                            <Text style={{fontSize: 17}}>{user.email}</Text>
                                        </View>
                                        <View style={{flexDirection: 'row', marginTop: 0}}>
                                            <View style={{width: "50%"}}>
                                                <Text style={{fontWeight: 'bold', fontSize: 17}}>Останній номер
                                                    телефону:</Text>
                                            </View>
                                            <Text style={{fontSize: 17}}>{user.last_number}</Text>
                                        </View>
                                        <View style={{flexDirection: 'row', marginTop: 0}}>
                                            <View style={{width: "50%"}}>
                                                <Text style={{fontWeight: 'bold', fontSize: 17}}>Коментар:</Text>
                                            </View>
                                            <Text style={{fontSize: 17}}>{user.comment}</Text>
                                        </View>
                                    </View>
                                ))
                            }
                        </>
                    }
                    <View style={{height: 150}}/>
                </ScrollView>
            </View>
            <MyAwesomeAlert
                showAlert={showAlert}
                title={'Попередження'}
                errorMessage={errorMessage}
                showConfirmButton={false}
                cancelText={'Закрити'}
                hideAlert={hideAlert}
            />
        </SafeAreaView>
    )
}
