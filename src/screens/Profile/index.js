import React, {useContext, useState} from "react";
import {
    Image,
    Keyboard,
    SafeAreaView,
    ScrollView,
    Text,
    TextInput,
    TouchableOpacity,
    TouchableWithoutFeedback,
    View
} from "react-native";
import TopMenu from "../../components/navidation/topMenu";
import {DatePicker, Input} from "../../components/Form";
import {launchImageLibrary} from "react-native-image-picker";
import AntDesign from "react-native-vector-icons/AntDesign";
import Lightbox from "react-native-lightbox-v2";
import {AuthContext} from "../../firebaseAuth/AuthProvider";
import {firebase} from "@react-native-firebase/auth";

export default function Profile() {
    const {logout} = useContext(AuthContext);
    let user = firebase.auth().currentUser;

    const [name, setName] = useState('');
    const [surname, setSurname] = useState('');
    const [patronymic, setPatronymic] = useState('');
    const [birthDate, setBirthDate] = useState(new Date().getTime());
    const [contacts, setContacts] = useState('');
    const [comment, setComment] = useState(' ');
    const [photoList, setPhotoList] = useState([]);
    const [email, setEmail] = useState(user.email);
    const [focus, setFocus] = useState(false);

    function saveAllData() {

    }

    function logoutUser() {
        logout(user.uid);
    }

    return (
        <SafeAreaView style={{backgroundColor: '#c4e4ef'}}>
            <TopMenu logout={logoutUser}/>
            <TouchableWithoutFeedback
                accessible={false}
                onPress={() => {
                    Keyboard.dismiss();
                }}>
                <View>
                    <ScrollView style={{margin: 10, height: '100%'}}>
                        <View style={{flexDirection: 'row'}}>
                            <View style={{marginTop: 25, width: 140, right: 10}}>
                                {photoList.length === 0 ?
                                    <TouchableOpacity
                                        style={{
                                            borderRadius: 5,
                                            borderWidth: 1,
                                            marginLeft: 10,
                                            borderColor: '#ACACAC',
                                            height: 160,
                                            alignItems: 'center',
                                            justifyContent: 'center'
                                        }}
                                        onPress={async () => {
                                            let options = {mediaType: 'photo', selectionLimit: 10}
                                            // @ts-ignore
                                            const result = await launchImageLibrary(options);
                                            if (result.didCancel !== true) {
                                                // @ts-ignore
                                                setPhotoList(result.assets);
                                            }
                                        }}
                                    >
                                        <AntDesign name={"pluscircleo"} size={28}/>
                                    </TouchableOpacity>
                                    :
                                    <View style={{}}>
                                        <Lightbox
                                            renderContent={() => (
                                                <View style={{}}>
                                                    <Image
                                                        style={{flex: 1}}
                                                        source={{uri: photoList[0].uri}}
                                                        resizeMode={"contain"}
                                                    />
                                                </View>
                                            )}>
                                            <View style={{
                                                flexDirection: 'row',
                                                bottom: 6
                                            }}>
                                                <Image
                                                    style={{
                                                        height: 160,
                                                        width: 140
                                                    }}
                                                    source={{uri: photoList[0].uri}}
                                                    resizeMode={"stretch"}
                                                />
                                                <TouchableOpacity
                                                    style={{position: 'absolute', right: 7, top: 5}}
                                                    onPress={() => {
                                                        setPhotoList([]);
                                                    }}
                                                >
                                                    <AntDesign name={'closecircleo'}
                                                               size={20}
                                                               color={'red'}/>
                                                </TouchableOpacity>
                                            </View>
                                        </Lightbox>
                                    </View>
                                }
                            </View>
                            <View style={{flexDirection: 'column', width: '100%'}}>
                                <View>
                                    <Input
                                        keyboardType="default"
                                        name="name"
                                        label={"Ім'я"}
                                        value={name}
                                        onValue={(name) => {
                                            setName(name);
                                        }}
                                    />
                                </View>
                                <View>
                                    <Input
                                        keyboardType="default"
                                        name="surname"
                                        label={"Прізвище"}
                                        value={surname}
                                        onValue={(surname) => {
                                            setSurname(surname);
                                        }}
                                    />
                                </View>
                                <View>
                                    <Input
                                        keyboardType="default"
                                        name="patronymic"
                                        label={"По-батькові"}
                                        value={patronymic}
                                        onValue={(patronymic) => {
                                            setPatronymic(patronymic);
                                        }}
                                    />
                                </View>
                            </View>
                        </View>
                        <View style={{marginTop: 10, right: 10}}>
                            <DatePicker
                                name="date"
                                label={'Дата народження'}
                                value={birthDate}
                                onValue={(birthDate) => {
                                    setBirthDate(birthDate);
                                }}
                                dateFormat={'DD.MM.YYYY'}
                            />
                        </View>
                        <View>
                            <Input
                                keyboardType="default"
                                name="contacts"
                                label={"Останній номер телефону"}
                                value={contacts}
                                onValue={(contacts) => {
                                    setContacts(contacts);
                                }}
                            />
                        </View>
                        <View>
                            <Input
                                keyboardType="email-address"
                                name="email"
                                label={"Електронна пошта"}
                                value={email}
                                onValue={(email) => {
                                    setEmail(email);
                                }}
                            />
                        </View>
                        <View style={{marginTop: 10}}>
                            <Text style={{color: focus ? 'dodgerblue' : 'gray'}}>Коментар або додаткова
                                інформація:</Text>
                            <View style={{
                                borderWidth: 1,
                                marginTop: 10,
                                borderColor: '#ACACAC',
                                justifyContent: 'flex-end',
                                flexDirection: 'column'
                            }}>

                                <TextInput
                                    keyboardType="default"
                                    value={comment}
                                    maxLength={1000}
                                    multiline={true}
                                    numberOfLines={6}
                                    style={{
                                        color: 'gray',
                                        height: 140,
                                        textAlignVertical: 'top'
                                    }}
                                    onFocus={() => {
                                        setFocus(true)
                                    }}
                                    onBlur={() => {
                                        setFocus(false)
                                    }}
                                    onChangeText={(comment) => {
                                        setComment(comment);
                                    }}
                                />
                            </View>
                        </View>
                        <View style={{width: "100%", alignItems: 'center', marginTop: 20}}>
                            <TouchableOpacity
                                style={{
                                    width: '90%',
                                    backgroundColor: 'dodgerblue',
                                    height: 40,
                                    justifyContent: "center",
                                    alignItems: 'center',
                                    borderRadius: 5
                                }}
                                onPress={() => {
                                    saveAllData();
                                }}>
                                <Text style={{fontSize: 17, fontWeight: 'bold', color: 'white'}}>Зберегти</Text>
                            </TouchableOpacity>
                        </View>
                    </ScrollView>
                </View>
            </TouchableWithoutFeedback>
        </SafeAreaView>
    )
}
