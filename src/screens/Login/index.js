import React, {useContext, useEffect, useState} from "react";
import {
    Image,
    KeyboardAvoidingView,
    Platform,
    SafeAreaView,
    StyleSheet,
    Text,
    TextInput,
    TouchableOpacity,
    View,
} from "react-native";
import auth from '@react-native-firebase/auth';
import {AuthContext} from "../../firebaseAuth/AuthProvider";
import Ionicons from "react-native-vector-icons/Ionicons";
import MyAwesomeAlert from "../../components/Form/AwesomeAlert/index";
import styled from "styled-components";
import clog from "../../utils/clog";

export function Login({navigation}) {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [passwordClosed, setPasswordClosed] = useState(true);
    const [iconName, setIconName] = useState('eye-off');
    const [initializing, setInitializing] = useState(true);
    const [showAlert, setShowAlert] = useState(false);
    const [errorMessage, setErrorMessage] = useState('');

    const {login} = useContext(AuthContext);

    useEffect(() => {
        return auth().onAuthStateChanged(onAuthStateChanged); // unsubscribe on unmount
    }, []);


    function onAuthStateChanged() {
        if (initializing) setInitializing(false);
    }

    function goToCreateAccountScreen() {
        navigation.navigate('CreateAccount');
    }

    function onLogin() {
        if (email === '') {
            showingAlert("Будь-ласка введіть email");
            return;
        }
        if (validate(email) === false) {
            showingAlert("Будь-ласка введіть правильний email");
            return;
        }
        if (password === '') {
            showingAlert("Будь-ласка введіть пароль");
            return;
        }
        try {
            login(email.toString().trim(), password, showingAlert);
        } catch (e) {
            clog(e)
            clog('e---')
        }
    }

    function validate(email) {
        let reg = /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w\w+)+$/;
        return reg.test(email)
    }

    function changePasswordIcon() {
        if (passwordClosed === true) {
            setPasswordClosed(false);
            setIconName('eye');
        } else {
            setPasswordClosed(true);
            setIconName('eye-off');
        }
    }

    function hideAlert() {
        setShowAlert(!showAlert)
    }

    function showingAlert(message) {
        setShowAlert(true);
        setErrorMessage(message);
    }

    return (
        <Container>
            <View>
                <View style={{
                    alignItems: "center",
                    marginTop: 72,
                    flexDirection: 'row',
                    justifyContent: 'center',
                    width: "100%"
                }}>
                    <Image
                        source={require("../../assets/logo.png")}
                        style={{height: 30, width: 30}}/>
                    <Text style={{ fontSize: 20,
                        marginLeft: 5,
                        marginBottom: 3,
                        fontWeight: "bold",
                        color: 'black'}}>
                        <Text style={{color: '#2b75e5'}}>U</Text>
                        <Text style={{color: '#f9d548'}}>A</Text> Family
                    </Text>
                </View>
                <View style={{alignItems: "center", marginTop: 50}}>
                    <Text style={{fontSize: 24}}>{"Вхід"}</Text>
                </View>
            </View>
            <KeyboardAvoidingView
                behavior={Platform.OS === "ios" ? "padding" : null}
                keyboardVerticalOffset={Platform.OS === "ios" ? 0 : 0}>
                <InputView>
                    <TextInput
                        value={email}
                        onChangeText={(email) => setEmail(email)}
                        placeholder={'Email'}
                        placeholderTextColor={'gray'}
                        style={styles.input}
                        keyboardType={'email-address'}
                    />
                </InputView>
                <View style={{flexDirection: "row"}}>
                    <InputView>
                        <TextInput
                            value={password}
                            onChangeText={(password) => setPassword(password)}
                            placeholder={'Пароль'}
                            placeholderTextColor={'gray'}
                            style={styles.input}
                            secureTextEntry={passwordClosed}
                        />
                        <TouchableOpacity
                            style={{position: 'absolute', right: 40, top: 10}}
                            onPress={() => {
                                changePasswordIcon()
                            }}>
                            <Ionicons name={iconName} size={25}/>
                        </TouchableOpacity>
                    </InputView>

                    {/*<View style={{position: "absolute", right: 10, paddingTop: 40}}>*/}
                    {/*    <Text style={{color: 'dodgerblue'}}>Forgot password?</Text>*/}
                    {/*</View>*/}
                </View>
            </KeyboardAvoidingView>
            <BottomView>
                <SignInButton
                    onPress={() => {
                        onLogin();
                    }}>
                    <Text style={{fontSize: 17, color: 'white'}}>Ввійти</Text>
                </SignInButton>
                <View style={{marginBottom: 20, alignItems: "center", justifyContent: "center", flexDirection: "row"}}>
                    <Text style={{fontSize: 15}}>Немає аккаунту?</Text>
                    <TouchableOpacity onPress={() => {
                        goToCreateAccountScreen();
                    }}>
                        <Text style={{color: 'dodgerblue'}}> Створити аккаунт</Text>
                    </TouchableOpacity>
                </View>
            </BottomView>
            <MyAwesomeAlert showAlert={showAlert}
                            title={'Попередження'}
                            errorMessage={errorMessage}
                            showConfirmButton={false}
                            cancelText={'Відмінити'}
                            hideAlert={hideAlert}
                            navigation={navigation}
                            titleStyle={{color: '#ffba00'}}
            />
        </Container>
    );
}

const SignInButton = styled(TouchableOpacity)`
  width: 86%;
  background-color: dodgerblue;
  margin-right: 24px;
  margin-left: 24px;
  justify-content: center;
  align-items: center;
  height: 50px;
  border-radius: 4px;
  margin-bottom: 10px;
`;

const BottomView = styled(View)`
  width: 100%;
  justify-content: center;
  align-items: center;
`;

const InputView = styled(View)`
  margin-top: 15px;
  margin-left: 24px;
  width: 100%;
`;

const Container = styled(SafeAreaView)`
  justify-content: space-between;
  flex: 1;
  background-color: #c4e4ef;
`;

const styles = StyleSheet.create({
    input: {
        paddingTop: 10,
        marginRight: 34,
        paddingBottom: 10,
        borderBottomWidth: 1,
        borderColor: "silver",
        marginBottom: 30,
        borderRadius: 7,
        flexDirection: "row",
        alignItems: "center"
    }
});




