export const HOST = 'https://web.agrocontrol.net';
export const DATE_FORMAT = 'DD.MM.Y HH:mm';
export const TIMEOUT = 60000;
export const constPointRefreshTime = 10;
