import React, {createContext, useState} from "react";
import auth from "@react-native-firebase/auth";
import clog from "../utils/clog";

export const AuthContext = createContext();
export const AuthProvider = ({children}) => {
    const [user, setUser] = useState();

    return (
        <AuthContext.Provider
            value={{
                user,
                setUser,
                login: async (email, password) => {
                    try {
                        await auth().signInWithEmailAndPassword(email, password)
                    } catch (err) {
                        if (err.toString().includes('user-not-found')) {
                            return 'userNotFound';
                        } else if (err.toString().includes('wrong-password')) {
                            return 'wrongPassword';
                        } else {
                            return 'pleaseTryLater';
                        }
                    }
                },
                register: async (email, password) => {
                    try {
                        await auth().createUserWithEmailAndPassword(email, password)
                    } catch (err) {
                        if (err.toString().includes('weak-password')) {
                            return 'userAlreadyExist';
                        } else if (err.toString().includes('email-already-in-use')) {
                            return 'userAlreadyExist';
                        } else {
                            return 'pleaseTryLater';
                        }
                    }
                },
                logout: async () => {
                    try {
                        await auth().signOut();
                    } catch (err) {
                        clog(err)
                        clog('err in signOut')
                    }
                },
            }}
        >
            {children}
        </AuthContext.Provider>
    );
};
