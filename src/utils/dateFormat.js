import moment from "moment";

export function dateFormat(date, format) {
    if(date) {
        return moment(date).format(format)
    }else {
        return ""
    }
}
